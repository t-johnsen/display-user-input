﻿using System;
using System.Dynamic;

namespace display_user_input
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello, you!");
            bool failure = true;

            Console.WriteLine("Enter the number 1 for SQUARE or number 2 for RECTANGLE, please.");

            // Handling user input for options
            while (failure)
            {
                string option = Console.ReadLine();
                Console.WriteLine("");

                if (String.Compare(option, "1") == 0)
                {
                    Console.WriteLine("Option 1: SQUARE");
                    Square();
                    failure = false;
                }
                else if (String.Compare(option, "2") == 0)
                {
                    Console.WriteLine("Option 2: RECTANGLE");
                    Rectangle();
                    failure = false;
                }
                else
                {
                    Console.WriteLine("You did not enter a correct number. Please try again");
                }

            }


        }

        // Top level method for interaction and printing square to user
        static private void Square()
        {

            Console.WriteLine("Enter the size of your square:");

            int number = InputErrorHandling();

            Print(number, number);

        }

        // Top level method for interaction and printing rectangle to user
        static private void Rectangle()
        {

            Console.WriteLine("Enter a size of one side of your rectangle:");
            int number1 = InputErrorHandling();

            Console.WriteLine("Enter a size of the other side of your rectangle:");
            int number2 = InputErrorHandling();

            Print(number1, number2);

        }

        // Generalized method for printing square and rectangel
        static private void Print(int number1, int number2)
        {
            for (int a = 0; a < number1; a++)
            {
                if(a == 0 || a == (number1 - 1))
                {
                    for (int i = 0; i < number2; i++)
                    {
                        Console.Write("*");
                    }
                }
                else
                {
                    Console.Write("*");
                    for(int i = 1; i <( number2-1); i++)
                    {
                        Console.Write(" ");
                    }
                    Console.Write("*");
                }

                Console.WriteLine("");
            }
        }

        // Error handling and user interaction method. Fetching numbers provided by user
        static private int InputErrorHandling()
        {
            bool failure = true;
            int number = 0;
            while (failure)
            {
                try
                {
                    number = int.Parse(Console.ReadLine());
                    failure = false;

                }
                catch (Exception)
                {
                    Console.WriteLine("You did not enter a number. Please try again.");

                }
            }
            return number;
        }

    }
}
